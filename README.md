# Angular Jest Template

> Originally cloned from [this repo](https://github.com/ahnpnl/jest-angular) and removed missing `test.ts` reference.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.4 and add `Jest` as testing framework.

Please check `jest-preset-angular` to see more details about the preset to work with Angular project.
